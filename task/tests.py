import datetime
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase
from .models import Task
from account.models import User


class TaskTest(APITestCase):
    user, created = User.objects.get_or_create(id=1)

    def test_list_tasks(self):
        self.client.force_authenticate(user=self.user)
        url = reverse('tasks_list')
        resp = self.client.get(url, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        self.assertEqual(resp.json(), [])

    def test_update_delete_task(self):
        task = Task.objects.create(name='Test', description='Test', deadline=datetime.datetime.now(), creator=self.user)
        task.save()
        self.client.force_authenticate(user=self.user)
        url = reverse('tasks_update_delete', args=[task.id])
        data = {'name': 'Test name', 'description': 'Test descr', 'deadline': '2022-05-05 13:00:00', 'file': ''}
        resp = self.client.put(url, data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)
        resp = self.client.delete(url, format='json')
        self.assertEqual(resp.status_code, status.HTTP_200_OK)

    def test_create_task(self):
        self.client.force_authenticate(user=self.user)
        url = reverse('tasks_list')
        data = {'name': 'Test name', 'description': 'Test descr', 'deadline': '2022-05-05 13:00:00', 'file': ''}
        resp = self.client.post(url, data, format='json')
        self.assertEqual(resp.status_code, status.HTTP_201_CREATED)
