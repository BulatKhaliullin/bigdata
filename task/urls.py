from django.urls import path
from .views import *


urlpatterns = [
    # Use this endpoint to get list of tasks by method GET and create new task by method POST
    path('list/', TasksListAPIView.as_view(), name='tasks_list'),
    # Use this endpoint to delete task by method DELETE, update - PUT, get current task - GET
    path('update_delete/<int:pk>/', TaskUpdateDeleteAPIView.as_view(), name='tasks_update_delete'),
    path('list/<int:pk>/', UserTasksListAPIView.as_view(), name='users_tasks_list'),
]
