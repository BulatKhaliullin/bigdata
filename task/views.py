from .serializers import *
from rest_framework.generics import ListCreateAPIView, RetrieveUpdateDestroyAPIView, ListAPIView, get_object_or_404
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.parsers import FileUploadParser
from rest_framework import status
from rest_framework.serializers import ValidationError
from rest_framework.pagination import LimitOffsetPagination


class UserTasksListAPIView(ListAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated, ]
    pagination_class = LimitOffsetPagination

    def list(self, request, *args, **kwargs):
        queryset = Task.objects.filter(creator__id=request.user.id)
        serializer = TaskSerializer(queryset, many=True)
        return Response(serializer.data, status=status.HTTP_200_OK)


class TasksListAPIView(ListCreateAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated, ]
    pagination_class = LimitOffsetPagination

    def perform_create(self, serializer):
        creator = get_object_or_404(User, id=self.request.user.id)
        return serializer.save(creator=creator)


class TaskUpdateDeleteAPIView(RetrieveUpdateDestroyAPIView):
    queryset = Task.objects.all()
    serializer_class = TaskSerializer
    permission_classes = [IsAuthenticated, ]
    parser_classes = [FileUploadParser, ]

    def perform_update(self, serializer):
        if serializer.data.get('creator', ''):
            if self.request.user.id != serializer.data.get('creator'):
                raise ValidationError('You have no permissions for delete this task')

    def perform_destroy(self, instance):
        if instance.creator:
            if self.request.user.id != instance.creator.id:
                raise ValidationError('You have no permissions for delete this task')
        else:
            raise ValidationError('You have no permissions for delete this task')
