from django.db import models
from account.models import User


class Task(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    deadline = models.DateTimeField()
    file = models.FileField(upload_to='uploads/', null=True, blank=True)
    creator = models.ForeignKey(User, on_delete=models.SET_NULL, null=True, blank=True, related_name='created_tasks')
    contributors = models.ManyToManyField(User, null=True, blank=True, related_name='tasks')

