from django.urls import path
from .views import *


urlpatterns = [
    path('registration/', RegistrationAPIView.as_view()),
    path('login/', LoginAPIView.as_view()),
    path('user_update/', UserRetrieveUpdateAPIView.as_view()),
]
