from rest_framework import serializers
from django.contrib.auth import authenticate
from .models import User


class UserSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=200, min_length=8, write_only=True)

    class Meta:
        model = User
        fields = ['email', 'username', 'full_name', 'password', 'token']
        read_only_fields = ['token', ]

    def update(self, instance, validated_data):
        password = validated_data.pop('password', None)
        for key, value in validated_data.items():
            setattr(instance, key, value)

        if password is not None:
            instance.set_password(password)

        instance.save()
        return instance


class RegistrationSerializer(serializers.ModelSerializer):
    password = serializers.CharField(max_length=128, min_length=8, write_only=True)
    token = serializers.CharField(max_length=200, read_only=True)

    class Meta:
        model = User
        fields = ['username', 'email', 'password', 'token', 'full_name']

    def create(self, validated_data):
        return User.objects.create_user(**validated_data)


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField(max_length=200)
    username = serializers.CharField(max_length=200, read_only=True)
    password = serializers.CharField(max_length=200, write_only=True)
    token = serializers.CharField(max_length=200, read_only=True)

    def validate(self, data):
        email = data.get('email', None)
        password = data.get('password', None)

        if email is None:
            raise serializers.ValidationError('Email is required for log in')

        if password is None:
            raise serializers.ValidationError('Password is required for log in')

        user = authenticate(username=email, password=password)

        if user is None:
            raise serializers.ValidationError('User with this email and password doesnt exist')

        if not user.is_active:
            raise serializers.ValidationError('This user is deactivated')

        return {'email': user.email, 'username': user.username, 'token': user.token}

